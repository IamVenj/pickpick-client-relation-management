/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import swal from 'sweetalert'
import VModal from 'vue-js-modal'
import Datepicker from 'vuejs-datepicker'
import vue2Dropzone from 'vue2-dropzone'
import 'vue2-dropzone/dist/vue2Dropzone.min.css'

window.Vue = require('vue');
Vue.use(VModal)
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('vue2Dropzone', vue2Dropzone);
Vue.component('Datepicker', Datepicker);
Vue.component('login-component', require('./components/LoginComponent.vue').default);
Vue.component('assistant-component', require('./components/AssistantComponent.vue').default);
Vue.component('assistant-edit-modal', require('./components/modal/AssistantEditModalComponent.vue').default);
Vue.component('assistant-delete-modal', require('./components/modal/AssistantDeleteModalComponent.vue').default);
Vue.component('client-component', require('./components/ClientComponent.vue').default);
Vue.component('view-client-component', require('./components/ViewClientComponent.vue').default);
Vue.component('view-client-document-component', require('./components/ViewClientDocumentComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
