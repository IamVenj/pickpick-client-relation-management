@extends('layouts.app')

@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
	    <div class="app-page-title">
	        <div class="page-title-wrapper">
	            <div class="page-title-heading">
	                <div class="page-title-icon">
	                    <i class="pe-7s-user icon-gradient bg-warm-flame">
	                    </i>
	                </div>
	                <div>Assistant
	                    <div class="page-title-subheading">List of added Assistants </div>
	                </div>
	            </div>
	        </div>
	    </div>            
	    <assistant-component></assistant-component>
	</div>
    @include('layouts.footer')
</div>

@endsection