@extends('layouts.app')

@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
	    <view-client-component :client="{{ $client }}"></view-client-component>
	</div>
    @include('layouts.footer')
</div>

@endsection