@extends('layouts.app')

@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
	    <view-client-document-component :client="{{ $client }}"></view-client-document-component>
	</div>
    @include('layouts.footer')
</div>

@endsection