<div class="app-sidebar sidebar-shadow">
    <div class="app-header__logo">
        <div class="logo-src"></div>
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>    
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading"></li>
                 <li class="@if(Request::is('dashboard')) mm-active @endif">
                    <a href="/dashboard" class="@if(Request::is('dashboard')) mm-active @endif">
                        <i class="metismenu-icon pe-7s-rocket"></i>
                        Dashboard
                    </a>
                </li>
                @if(auth()->user()->role === 1 )
                <li class="@if(Request::is('user')) mm-active @endif">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-user"></i>
                        User
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="/user" class="@if(Request::is('user')) mm-active @endif">
                                <i class="metismenu-icon">
                                </i>Assistant
                            </a>
                        </li>
                    </ul>
                </li>
                @endif
                <li class="@if(Request::is('client')) mm-active @endif">
                    <a href="/client" class="@if(Request::is('client')) mm-active @endif">
                        <i class="metismenu-icon pe-7s-users"></i>
                        Client
                    </a>
                </li>
                @if(auth()->user()->role === 1)
                <li class="@if(Request::is('/history/loan')) mm-active @endif">
                    <a href="/history/loan" class="@if(Request::is('history/loan')) mm-active @endif">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Loan History
                    </a>
                </li>
                <li class="@if(Request::is('/history/payment')) mm-active @endif">
                    <a href="/history/payment" class="@if(Request::is('history/payment')) mm-active @endif">
                        <i class="metismenu-icon pe-7s-cash"></i>
                        Payment History
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
</div>    