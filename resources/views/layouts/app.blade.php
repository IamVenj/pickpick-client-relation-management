<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <script src="{{ asset('js/app.js') }}" defer></script>
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700|Montserrat:400,700&display=swap" rel="stylesheet"> 
    <link href="{{ asset('css/main.css') }}" rel="stylesheet"></head>
</head>
<body>
    <div id="app">
        <div class="app-container app-theme-white body-tabs-shadow @if(!Request::is('login')) fixed-header fixed-sidebar @endif">
            @if(!Request::is('login'))
            @include('layouts.top-nav')
            <div class="app-main">
                @include('layouts.sidebar')
            @endif
                @yield('content')
            @if(!Request::is('login'))
            </div>
            @endif
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.min.js"></script>
</body>
</html>
