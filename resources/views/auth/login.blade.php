@extends('layouts.app')

@section('content')
<div class="app-container">
    <div class="h-100 bg-plum-plate bg-animation">
        <div class="d-flex h-100 justify-content-center align-items-center">
            <div class="mx-auto app-login-box col-md-8">
                <div class="app-logo-inverse mx-auto mb-3"></div>
                <div class="modal-dialog w-100 mx-auto">
                    <div class="modal-content">
                        
                        <login-component></login-component>

                    </div>
                </div>
                <div class="text-center text-white opacity-8 mt-3">Copyright © ArchitectUI 2019</div>
            </div>
        </div>
    </div>
</div>
@endsection
