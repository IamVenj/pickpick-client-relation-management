<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('home');
Route::get('/client', 'ClientController@index')->name('client');
Route::post('/client/create', 'ClientController@store')->name('client.store');
Route::get('/client/all', 'ClientController@retreive')->name('client.get');
Route::get('/client/{id}', 'ClientController@viewClient')->name('client.view');
Route::get('/client/{id}/documents', 'ClientController@viewDocument')->name('client.view.document');
Route::get('/user', 'USerController@index')->name('user');
Route::post('/user/create', 'USerController@create')->name('user.create');
Route::get('/user/all/assistants', 'USerController@retreive')->name('user.get');
Route::patch('/user/update/{id}', 'USerController@update')->name('user.update');
Route::delete('/user/destroy/{id}', 'USerController@destroy')->name('user.destroy');
