<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('photo_public_id')->nullable();
            $table->string('photo_version')->nullable();
            $table->integer('age');
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('phone_number');
            $table->string('bank_name');
            $table->bigInteger('account_number');
            $table->double('monthly_fee');
            $table->double('loan');
            $table->integer('taxi_code');
            $table->string('key');
            $table->integer('client_status')->default(0);
            // client_status >> Available - 0, Transfer - 1, terminated - 2.
            $table->string('authorization');
            $table->string('third_party_authorization')->nullable();
            $table->date('loan_deadline');
            $table->integer('cue')->nullable();
            $table->integer('loantype')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
