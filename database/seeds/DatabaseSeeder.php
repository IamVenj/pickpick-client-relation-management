<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
      	factory(App\User::class)->create([

	        'name' => 'Admin',
	        'email' => 'admin@admin.com',
	        'password' => bcrypt('pickpick_crm_admin'),
	        'role' => '1',

      	]);
    }
}
