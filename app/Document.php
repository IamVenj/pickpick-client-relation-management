<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = ['client_id', 'field', 'document'];

    protected $hidden = ['client_id'];

    public function createDocument($client_id, $documentArr){
    	for ($i=0; $i < count($documentArr); $i++) { 
    		$this::create([
    			'client_id' => $client_id,
    			'field' => $documentArr[$i]['type'],
    			'document' => $documentArr[$i]['signedUrl']
    		]);
    	}
    }
}
