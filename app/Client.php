<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Client extends Model
{
    protected $fillable = ['name', 'age', 'address1', 'address2', 'phone_number', 'bank_name', 'account_number', 'monthly_fee', 'loan', 'taxi_code', 'key', 'client_status', 'authorization', 'third_party_authorization', 'loan_deadline', 'cue', 'loantype'];

    public function documents() {
        return $this->hasMany('App\Document');
    }

    public function createClient($name, $age, $address1, $address2, $phone_number, $bank_name, $account_number, $monthly_fee, $loan, $taxi_code, $key, $client_status, $authorization, $third_party_authorization, $loan_deadline, $cue, $loantype)
    {
    	$client = $this::create([

    		'name' => $name,
    		'age' => $age,
    		'address1' => $address1,
    		'address2' => $address2,
    		'phone_number' => $phone_number,
    		'bank_name' => $bank_name,
    		'account_number' => $account_number,
    		'monthly_fee' => $monthly_fee,
    		'loan' => $loan,
    		'taxi_code' => $taxi_code,
    		'key' => $key,
    		'client_status' => $client_status,
    		'authorization' => $authorization,
    		'third_party_authorization' => $third_party_authorization,
    		'loan_deadline' => $this->convertDate($loan_deadline),
    		'cue' => $cue,
    		'loantype' => $loantype

    	]);

    	return $client;
    }

    private function convertDate($_date) {
		return date('Y-m-d', strtotime($_date));
    } 

    public function updateClient($id) {
        $client = $this::find($id);
    }

    public function destroyClient($id) {
        $this::find($id)->delete();
    }

}
