<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class USerController extends Controller
{
	private $_user;
    public function __construct() {
    	$this->middleware(['auth', 'admin']);
    	$this->_user = new User();
    }

    public function index() {
    	return view('pages.user.index');
    }

    public function create(Request $request) {
    	$this->validate(request(), [
    		'name' => 'required',
    		'email' => 'required|unique:users',
    		'password' => 'required'
    	]);

    	$name = $request->name;
    	$email = $request->email;
    	$password = $request->password;

    	$assistant = $this->_user->createAssistant($name, $email, $password);
    	return response()->json(['username'=>$name, 'message'=>'Account successfully created', 'assistant' => $assistant], 200);
    }

    public function retreive() {
    	return $this->_user::where('role', 2)->get();
    }

    public function update($id, Request $request) {
    	$this->validate(request(), [
    		'editEmail' => 'required',
    		'editName' => 'required'
    	]);

    	$name = $request->editName;
    	$email = $request->editEmail;
    	$password = $request->password;

    	$this->_user->updateAssistant($name, $email, $password, $id);
    	return response()->json(['username'=>$name, 'message'=>'Account successfully updated'], 200);
    } 

    public function destroy($id) {
    	$this->_user->destroyUser($id);
    	return response()->json(['message'=>'Successfully deleted!'], 200);
    }
}
