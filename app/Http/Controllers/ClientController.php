<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Document;
use App\Client;

class ClientController extends Controller
{
	private $_client, $_document;
    public function __construct() {
    	$this->middleware('auth');
    	$this->_document = new Document();
    	$this->_client = new Client();
    }

    public function index() {
    	return view('pages.client.index');
    }

    public function store(Request $request) {
		$this->validate(request(), [
			'name' => 'required',
			'age' => 'required',
			'address1' => 'required',
			'phone_number' => 'required',
			'bank_name' => 'required',
			'account_number' => 'required',
			'monthly_fee' => 'required',
			'loan' => 'required',
			'taxi_code' => 'required',
			'key' => 'required',
			'client_status' => 'required',
			'authorization' => 'required',
			'loan_deadline' => 'required',
			'cue' => 'required',
			'loantype' => 'required'
		]);
		
		$client = $this->_client->createClient($request->name, $request->age, $request->address1, $request->address2, $request->phone_number, $request->bank_name, $request->account_number, $request->monthly_fee, $request->loan, $request->taxi_code, $request->key, $request->client_status, $request->authorization, $request->third_party_authorization, $request->loan_deadline, $request->cue, $request->loantype);
		$this->_document->createDocument($client->id, $request->documents);

		return response()->json(['client'=>$client, 'client_name'=>$request->name, 'message'=>"Client is Successfully Added"], 200);
    }

    public function retreive() {
    	return $this->_client::all();
    }

    public function viewClient($id) {
    	$client = $this->_client::find($id);
    	return view('pages.client.view', compact('client'));
    }

    public function viewDocument($id) {
    	$client = $this->_client::with(['documents'])->find($id);
    	return view('pages.client.view-document', compact('client'));
    }

    public function update($id, Request $request) {
    	$this->validate(request(), [

    	]);
    	$this->_client->updateClient($id);
    	return response()->json([], 200);
    }

    public function destroy($id) {
    	$this->_client->destroyClient($id);
    	return response()->json(['message' => 'Successfully deleted!'], 200);
    }
}
