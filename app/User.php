<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function createAssistant($name, $email, $password) {
        $assistant = $this::create([

            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
            'role' => 2
        
        ]);
        return $assistant;
    }

    public function updateAssistant($name, $email, $password, $id) {
        $assistant = $this::find($id);
        $assistant->name = $name;
        $assistant->email = $email;
        $assistant->password = bcrypt($password);
        $assistant->save();
    }

    public function destroyUser($id) {
        $this::find($id)->delete();
    }
}
